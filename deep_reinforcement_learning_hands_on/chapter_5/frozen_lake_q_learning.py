#!/usr/bin/env python3
"""Simple value iteration / q learning agent"""
import numpy as np
from collections import defaultdict, Counter
import gym
import time


class ValueIterationAgent:
    """Haha no 'object'! :) Implements a value iteration agent"""
    def __init__(self, env, gamma):
        self.env = env
        self.gamma = gamma
        # Initialise the environment... why do we need it in the agent?
        self.state = self.env.reset()
        # here we store the rewards we got historically
        self.rewards = defaultdict(float)
        # here we store the transitions we made
        self.transits = defaultdict(Counter)
        # finally, state values - we are doing value iteration after all!
        self.values = defaultdict(float)

    def play_n_random_steps(self, count):
        """Why..?"""
        for _ in range(count):
            action = self.env.action_space.sample()
            new_state, reward, is_done, _ = self.env.step(action)
            # I guess we couldn't do it if rewards were random, but here we can
            self.rewards[(self.state, action, new_state)] = reward
            # update the information that we have transitioned to this state
            self.transits[(self.state, action)][new_state] += 1
            # continue or go back
            self.state = (self.env.reset() if is_done else new_state)

    def calc_action_value(self, state, action):
        """Q value approximation"""
        # where we arrived historically from this state, action
        target_counts = self.transits[(state, action)]
        total = sum(target_counts.values())

        action_value = 0.0
        # go through all destinations we have seen, the rewards we got from them,
        # and the probability of hitting them given this state, action
        for tgt_state, count in target_counts.items():
            reward = self.rewards[(state, action, tgt_state)]
            # remember, true division... :/
            action_value += (count / total) * (
                reward
                + self.gamma*self.values[tgt_state])
        return action_value

    def select_action(self, state):
        """Given we are in a state, pick an action. Just argmax really (?)"""
        best_action, best_value = None, None
        for action in range(self.env.action_space.n):
            # pick out action values
            action_value = self.calc_action_value(state, action)
            if best_value is None or best_value < action_value:
                best_value, best_action = action_value, action
        return best_action

    def play_episode(self, external_env):
        """Evaluates current agent quality on an external environment"""
        total_reward = 0.0
        state = external_env.reset()
        while True:
            action = self.select_action(state)
            new_state, reward, is_done, _ = external_env.step(action)
            self.rewards[(state, action, new_state)] = reward
            self.transits[(state, action)][new_state] += 1
            total_reward += reward
            if is_done:
                break
            state = new_state
        return total_reward

    def value_iteration(self):
        """
        The path to enlightened self-improvement. New value is just largest
        Q at each point
        """
        # run for each state, improve the value function
        for state in range(self.env.observation_space.n):
            state_values = [self.calc_action_value(state, action)
                            for action in range(self.env.action_space.n)]
            self.values[state] = max(state_values)



class ValueActionAgent:
    """Now model the Q-function"""
    def __init__(self, env, gamma):
        self.env = env
        self.gamma = gamma
        self.state = self.env.reset()
        self.rewards = defaultdict(float)
        self.transits = defaultdict(Counter)
        # this is now values of state_action tuples, not states... right?
        self.values = defaultdict(float)

    def play_n_random_steps(self, count):
        # play truly at random, hope for the best
        for _ in range(count):
            action = self.env.action_space.sample()
        new_state, reward, is_done, _ = self.env.step(action)
        # this is just book-keeping, same as above; record the experience
        self.rewards[(self.state, action, new_state)] = reward
        self.transits[(self.state, action)][new_state] += 1
        self.state = self.env.reset() if is_done else new_state

    def select_action(self, state):
        # just argmax of Q(s, a) at this state
        best_action, best_value = None, -np.inf
        for action in range(self.env.action_space.n):
            action_value = self.values[(state, action)]
            if action_value > best_value:
                best_action, best_value = action, action_value
        return best_action

    def play_episode(self, test_env):
        """For testing the agent on an external episode"""
        total_reward = 0.0
        state = test_env.reset()
        while True:
            # state = test_env.reset()
            action = self.select_action(state)
            new_state, reward, is_done, _ = test_env.step(action)
            # again, book-keeping... is it a bit cheeky that we include this here?
            # maybe we shouldn't and I won't. NB the code sample
            # uses the test episodes to enhance experience
            # maybe we need those on-policy runs though?
            self.rewards[(state, action, new_state)] = reward
            self.transits[(state, action)][new_state] += 1
            total_reward += reward
            if is_done:
                break
            state = new_state
        return total_reward

    def value_iteration(self):
        """Here is bit where we learning!"""
        # iterate over states and actions
        for state in range(self.env.observation_space.n):
            for action in range(self.env.action_space.n):
                action_value = 0.0
                target_counts = self.transits[(state, action)]
                total_counts = sum(target_counts.values())

                for tgt_state, count in target_counts.items():
                    reward = self.rewards[(state, action, tgt_state)]
                    best_action = self.select_action(tgt_state)
                    action_value += (count/total_counts) * (
                        reward
                        + self.gamma * self.values[tgt_state, best_action])
                self.values[state, action] = action_value
            

def main():
    ENV='FrozenLake-v0'
    TEST_EPISODES = 20
    GAMMA = 0.9
    test_env = gym.make(ENV)
    # agent = ValueIterationAgent(gym.make(ENV), GAMMA)
    agent = ValueActionAgent(gym.make(ENV), GAMMA)

    iter_no = 0
    best_reward = 0.0

    # iterate over episodes
    while True:
        iter_no += 1
        # gain some more experience
        # note here we're not doing eps-greedy or anything, just
        # doing random steps and hoping for the best
        agent.play_n_random_steps(100)
        agent.value_iteration()

        # evaluate the agent's progress
        reward = 0.0
        for _ in range(TEST_EPISODES):
            reward += agent.play_episode(test_env)
        reward /= TEST_EPISODES

        if reward > best_reward:
            print(f"Best reward updated {best_reward:.3f} -> {reward:.3f} at step {iter_no}")
            best_reward = reward
        if reward > 0.9:
            print(f'Solved in {iter_no} steps')
            break

    demo_env(agent, test_env, 0.5)

def demo_env(agent, env, sleep):
    state = env.reset()
    try:
        while True:
            action = agent.select_action(state)
            new_state, reward, is_done, _ = env.step(action)
            if is_done:
                state = env.reset()
            else:
                state = new_state

            env.render()
            time.sleep(sleep)
    finally:
        env.close()        
            
        
if __name__ == '__main__':
    main()
