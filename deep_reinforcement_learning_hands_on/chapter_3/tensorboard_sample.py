#!/usr/bin/env python
"""
Sadly not workee tensorboard. Need asks Ben for helps
"""

import math
from tensorboardX import SummaryWriter

def main():
    writer = SummaryWriter()
    funcs = {
        'sin' : math.sin,
        'cos' : math.cos,
        'tan' : math.tan,
        }

    for angle in range(-360, +360):
        # write various scalar messages
        angle_rad = angle * math.pi / 180.0

        for name, fun in funcs.items():
            val = fun(angle_rad)
            writer.add_scalar(name, val, angle)
    writer.close()

    

if __name__ == '__main__':
    main()
