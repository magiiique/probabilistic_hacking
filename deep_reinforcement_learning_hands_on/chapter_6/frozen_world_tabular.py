"""
Regular Q-learning, with directly learning Q function, no diddle daddle
with other state components
"""

import gym
from collections import defaultdict
import numpy as np

class Agent:
    def __init__(self, env, learning_epsilon, alpha, gamma):
        self.env = env
        self.state = self.env.reset()
        self.values = defaultdict(float)
        self.learning_epsilon = learning_epsilon
        self.alpha = alpha
        self.gamma = gamma

    def __str__(self):
        return f'Q-agent({self.learning_epsilon:.3f})'
        
    def sample_env(self):
        """This is for learning, and acts eps-greedy"""
        act_random = False
        if (self.learning_epsilon == 1.0):
            act_random = True
        elif (np.random.sample() <= self.learning_epsilon):
            # also act randomly...
            act_random = True

        old_state = self.state
        if act_random:
            action = self.env.action_space.sample()
        else:
            # do in a minute; pick optimal action instead
            _, action = self.best_value_and_action(self.state)
        new_state, reward, is_done, _ = self.env.step(action)
        self.state = (self.env.reset() if is_done else new_state)
        return (old_state, action, reward, new_state)

    def best_value_and_action(self, state):
        """Simples; pick largest Q"""
        # best_value, best_action = -np.inf, None
        # for action in range(self.env.action_space.n):
        #     action_value = self.values[state, action]
        #     if action_value > best_value:
        #         best_value = action_value
        #         best_action = action
        # return best_value, best_action
        values_actions = [
            (self.values[state, action], action)
            for action in range(self.env.action_space.n)]
        best_value = max(value for value, _ in values_actions)
        admissible_actions = [(value, action)
                              for value, action in values_actions
                              if value >= best_value]
        return admissible_actions[ np.random.choice(len(admissible_actions)) ]

    def value_update(self, s, a, r, next_s):
        """Does a single step of improvement"""
        best_value, _ = self.best_value_and_action(next_s)
        new_value = r + self.gamma*best_value
        old_value = self.values[s, a]
        self.values[s, a] = old_value*(1-self.alpha) + new_value * self.alpha

    def play_episode(self, env):
        """Plays an episode to test the agent"""
        total_reward = 0.0
        state = env.reset()
        is_done = False
        while not is_done:
            _, action = self.best_value_and_action(state)
            new_state, reward, is_done, _ = env.step(action)
            total_reward += reward
            state = new_state
        return total_reward


ENV_NAME='FrozenLake-v0'
TEST_EPISODES=20

def train_agent(agent):
    test_env = gym.make(ENV_NAME)

    iter_no = 0
    best_reward = 0.0
    while True:
        iter_no += 1
        for _ in range(100):
            s, a, r, next_s = agent.sample_env()
            agent.value_update(s, a, r, next_s)

        # test
        reward =0.0
        for _ in range(TEST_EPISODES):
            reward += agent.play_episode(test_env)
        reward /= TEST_EPISODES
        if reward > best_reward:
            print(f"Best reward updated {best_reward:.2f} -> {reward:.2f} at iteration {iter_no}")
            best_reward = reward
        if reward >= 0.8:
            print(f'Solved in {iter_no} iterations')
            break

    return agent, iter_no, reward

def main():
    def agent_factory(epsilon):
        def inner():
            return Agent(gym.make(ENV_NAME), epsilon, 0.2, 0.9)
        return inner
    agents_factories = [
        agent_factory(epsilon)
        for epsilon in [1.0, 0.5, 0.1, 0.05]]

    learning_times = []
    for agent in agents_factories:
        agent_times = []
        for _ in range(10):
            really_agent = agent()
            _, training_time, _ = train_agent(really_agent)
            agent_times.append(training_time)
        learning_times.append( np.mean(agent_times) )

    for agent, time in zip(agents_factories, learning_times):
        print("{}, {:.1f}".format(
            agent(), time))


if __name__ == '__main__':
    main()
