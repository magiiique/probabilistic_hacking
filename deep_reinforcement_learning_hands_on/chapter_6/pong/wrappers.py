"""
Env wrappers, all based on the samples
"""

import cv2
import gym
import gym.spaces
import numpy as np
import collections

from functools import partial

class FireResetEnv(gym.Wrapper):
    """Presses 'fire' at the start of the game"""
    def __init__(self, env):
        super(FireResetEnv, self).__init__(env)
        # make sure I am wrapping the correct action
        assert env.unwrapped.get_action_meanings()[1] == 'FIRE'
        # some kind of sanity test?
        assert len(env.unwrapped.get_action_meanings()) >= 3

    def step(self, action):
        return self.env.step(action)

    def reset(self):
        self.env.reset()
        # press 'FIRE'
        obs, _, done, _ = self.env.step(1)
        if done:
            self.env.reset()

        obs, _, done, _ = self.env.step(2)
        if done:
            self.env.reset()

        return obs


class MaxAndSkipEnv(gym.Wrapper):
    """Returns only every skip-th frame"""
    def __init__(self, env, skip=4):
        super(MaxAndSkipEnv, self).__init__(env)
        # observations buffer
        self._obs_buffer = collections.deque(maxlen=2)
        self._skip = skip

    def step(self, action):
        total_reward = 0.0
        done = None
        for _ in range(self._skip):
            # step this many times with the same action, collect rewards
            obs, reward, done, info = self.env.step(action)
            self._obs_buffer.append(obs)
            total_reward += reward
            if done:
                break
        max_frame = np.max(np.stack(self._obs_buffer), 0)
        return max_frame, total_reward, done, info

    def reset(self):
        """Reset, yeah"""
        # clear observation buffer
        self._obs_buffer.clear()
        # reset underlying env, get an observation
        obs = self.env.reset()
        # stick it as a start to our two-buffer max
        self._obs_buffer.append(obs)
        # return it, as we should
        return obs

class ProcessFrame84(gym.ObservationWrapper):
    """Munges and remunges the observations"""
    def __init__(self, env):
        super(ProcessFrame84, self).__init__(env)
        self.observation_space = gym.spaces.Box(
            low=0,
            high=255,
            shape=(84, 84, 1),
            dtype=np.uint8)

    def observation(self, obs):
        return ProcessFrame84.process(obs)

    @staticmethod
    def process(frame):
        if frame.size == 210*160*3:
            img = np.reshape(frame, [210, 160, 3]).astype(np.float32)
        elif frame.size == 250*160*3:
            img = np.reshape(frame, [250, 160, 3]).astype(np.float32)
        else:
            raise ValueError('Unknown resolution')

        img = (
            img
            * np.array([0.299, 0.587, 0.114])[np.newaxis, np.newaxis, :]).sum(-1)

        resized_screen = cv2.resize(img, (84, 110), interpolation=cv2.INTER_AREA)
        x_t = resized_screen[18:102, :]
        # make it 84 x 84 x 1 channel
        x_t = np.reshape(x_t, [84, 84, 1])
        return x_t.astype(np.uint8)

class ImageToPyTorch(gym.ObservationWrapper):
    def __init__(self, env):
        super(ImageToPyTorch, self).__init__(env)
        old_shape = self.observation_space.shape
        self.observation_space = gym.spaces.Box(
            low=0.0,
            high=1.0,
            shape=(
                old_shape[-1],
                old_shape[0],
                old_shape[1]),
            dtype=np.float32)

    def observation(self, observation):
        """Returns an observation, with the colour channel as the first axis"""
        return np.moveaxis(observation, 2, 0)

class ScaledFloatFrame(gym.ObservationWrapper):
    """Rescales the observations, apparently"""
    def observation(self, obs):
        return np.array(obs).astype(np.float32) / 255.0


class BufferWrapper(gym.ObservationWrapper):
    """Circular buffer of observations"""
    def __init__(self, env, n_steps, dtype=np.float32):
        super(BufferWrapper, self).__init__(env)
        self.dtype=dtype
        old_space = env.observation_space
        # repeat the obserational space
        self.observation_space = gym.spaces.Box(
            old_space.low.repeat(n_steps, 0),
            old_space.high.repeat(n_steps, 0),
            dtype=dtype)

    def reset(self):
        self.buffer = np.zeros_like(
            self.observation_space.low,
            dtype=self.dtype)
        return self.observation(self.env.reset())

    def observation(self, observation):
        # TODO isn't this quite inefficient?
        self.buffer[:-1] = self.buffer[1:]
        self.buffer[-1] = observation
        return self.buffer

def wrap_env(env):
    result = env
    for wrapper in [
            MaxAndSkipEnv,
            FireResetEnv,
            ProcessFrame84,
            ImageToPyTorch,
            partial(BufferWrapper, n_steps=4),
            ScaledFloatFrame,
            ]:
        result = wrapper(result)
    return result

