"""
Train a pong player jah!
"""

import wrappers, dqn_model

import argparse
import time
import numpy as np
import collections

import gym
import torch
import torch.nn as nn
import torch.optim as optim

# no tensorboard imports :/

Experience = collections.namedtuple(
    'Experience',
    ('state', 'action', 'reward', 'done', 'new_state'))


class ExperienceBuffer:
    """Experience replay buffer"""
    def __init__(self, capacity):
        self.buffer = collections.deque(maxlen=capacity)

    def __len__(self):
        return len(self.buffer)

    def append(self, experience):
        self.buffer.append(experience)

    def sample(self, batch_size):
        """Get samples from the replay buffer"""
        # hmm why not sample with replacement? Not that it matters much!
        # TODO investigate
        indices = np.random.choice(len(self.buffer), batch_size, replace=False)
        
        states, actions, rewards, dones, next_states = zip(*[
            self.buffer[idx]
            for idx in indices ])

        return (
            np.array(states),
            np.array(actions),
            np.array(rewards, dtype=np.float32),
            np.array(dones, dtype=np.uint8),
            np.array(next_states))


class Agent:
    def __init__(self, env, exp_buffer):
        self.env = env
        self.exp_buffer = exp_buffer
        self._reset()

    def _reset(self):
        self.state = self.env.reset()
        self.total_reward = 0.0

    def play_step(self, net, epsilon=0.0, device='cpu'):
        done_reward = None
        if np.random.random() < epsilon:
            # act random
            action = self.env.action_space.sample()
        else:
            # do best action
            state_a = np.array([self.state], copy=False)
            state_v = torch.tensor(state_a).to(device)
            q_vals_v = net(state_v)
            _, act_v = torch.max(q_vals_v, dim=1)
            action = int(act_v.item())

        # step in the environment
        new_state, reward, is_done, _ = self.env.step(action)
        self.total_reward += reward

        # add to the experience buffer
        exp = Experience(self.state, action, reward, is_done, new_state)
        self.exp_buffer.append(exp)

        self.state = new_state
        if is_done:
            done_reward = self.total_reward
            self._reset()
        return done_reward


def calc_loss(batch, net, tgt_net, device='cpu'):
    # unpack the batch
    states, actions, rewards, dones, next_states = batch

    # business on running stuff on the GPU... kinda useless here I guess!
    # but I'll keep it for experiments
    states_v = torch.tensor(states).to(device)
    next_states_v = torch.tensor(next_states).to(device)
    actions_v = torch.tensor(actions).to(device)
    rewards_v = torch.tensor(rewards).to(device)
    done_mask = torch.ByteTensor(dones).to(device)

    # err figure out what's going on here!
    state_action_values = net(states_v).gather(1, actions_v.unsqueeze(-1)).squeeze(-1)
    next_state_values = tgt_net(next_states_v).max(1)[0]
    next_state_values[done_mask] = 0.0
    next_state_values = next_state_values.detach()

    GAMMA = 0.99
    expected_state_action_values = next_state_values * GAMMA + rewards_v
    return nn.MSELoss()(state_action_values, expected_state_action_values)

def main():
    REPLAY_SIZE = 10000
    env = wrappers.wrap_env(gym.make('PongNoFrameskip-v4'))

    print(env.observation_space.shape)
    net = dqn_model.DQN(
        env.observation_space.shape,
        env.action_space.n)

    tgt_net = dqn_model.DQN(
        env.observation_space.shape,
        env.action_space.n)

    buffer = ExperienceBuffer(REPLAY_SIZE)
    agent = Agent(env, buffer)
    epsilon = 1.0

    LEARNING_RATE=1e-4
    optimiser = optim.Adam(net.parameters(), lr=LEARNING_RATE)
    total_rewards = []
    frame_idx = 0
    ts_frame = 0
    ts = time.time()
    best_mean_reward = None

    while True:
        frame_idx += 1
        epsilon = max(0.02, 1.0 - frame_idx / 100e3)

        reward = agent.play_step(net, epsilon)
        if reward is not None:
            total_rewards.append(reward)
            speed = (frame_idx - ts_frame) / (time.time() - ts)
            ts_frame = frame_idx
            ts = time.time()

            mean_reward = np.mean(total_rewards[-100:])
            print('{}: done {} games, mean reward {:.3f}, '
                  'eps {:.2f}, speed {:.1f} fps'.format(
                      frame_idx, len(total_rewards),
                      mean_reward, epsilon, speed))

            if best_mean_reward is None or best_mean_reward < mean_reward:
                torch.save(net.state_dict(), 'pong-best.dat')
                if best_mean_reward is not None:
                    print('Best reward {:.2f} -> {:.2f}'.format(
                        best_mean_reward, mean_reward))
                best_mean_reward = mean_reward

            # let's aim low for a start
            if mean_reward > 10.0:
                print('Solved in {} frames'.format(frame_idx))
                break

        if len(buffer) < 10000:
            # apparently no point starting now..?
            continue

        if frame_idx % 1000 == 0:
            # time to synch the networks
            tgt_net.load_state_dict(net.state_dict())

        # optimise the steps
        optimiser.zero_grad()
        batch = buffer.sample(32)
        loss_t = calc_loss(batch, net, tgt_net)
        loss_t.backward()
        optimiser.step()

if __name__ == '__main__':
    main()
