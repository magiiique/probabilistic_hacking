#!/usr/bin/env python3
"""
Making the cartpole example, first hackings
"""
import gym
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim

from tensorboardX import SummaryWriter
from collections import namedtuple

# import matplotlib.pyplot as plt
import time

# make a class defining the net
# eventually this will also be where I do modifications
class Net(nn.Module):
    # I'm ignoring input size, n_actions - this isn't general purpose
    def __init__(self, hidden_size, enforce_symmetry=False):
        super(Net, self).__init__()
        self.net = nn.Sequential(
            nn.Linear(4, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, 2),
            # here I would do softmax, but I'll wrap that into the objective instead
            )
        self.enforce_symmetry = enforce_symmetry

    def forward(self, x):
        if self.enforce_symmetry:
            # run on x, run on -1, invert stuff
            regular_run = self.net(x)
            index = torch.LongTensor([1, 0])
            inverted_run = self.net(-x)
            # return regular_run + inverted_run[::-1]
            return regular_run + inverted_run[:, index]
        else:
            return self.net(x)


# err just some C&P - data structures from the sample
Episode = namedtuple('Episode', field_names=['reward', 'steps'])
EpisodeStep = namedtuple('EpisodeStep', field_names=['observation', 'action'])



def generate_training_data(environment, net, batch_size):
    """Trains a given net in an environment"""
    batch = []
    episode_reward = 0.0
    episode_steps = []

    obs = environment.reset()
    sm = nn.Softmax(dim=1)

    while True:
        # get the observation
        obs_v = torch.FloatTensor([obs])

        # get action probabilities from the net being trained
        action_probabilities = sm(net(obs_v))

        action_probabilities_np = action_probabilities.data.numpy()[0]

        # generate an action for the agent
        action = np.random.choice(2, p=action_probabilities_np)

        # generate the next action
        next_observation, reward, is_done, _ = environment.step(action)

        # update the current episode
        episode_reward += reward
        episode_steps.append(EpisodeStep(observation=obs, action=action))

        if is_done:
            # we are done with the episode, let's tidy up and move on
            batch.append(Episode(reward=episode_reward, steps=episode_steps))
            episode_reward = 0.0
            episode_steps = []
            next_observation = environment.reset()

            # if we reached batch size, return
            if len(batch) == batch_size:
                yield batch
                batch = []
        # either way, this is the observation we are starting from next time
        obs = next_observation


def filter_batch(batch, percentile):
    """Excludes poor episodes, only keeps the good uns"""
    # get the raw rewards
    rewards = list(map(lambda s: s.reward, batch))
    reward_lower_bound = np.percentile(rewards, percentile)
    reward_mean = float(np.mean(rewards))

    train_observations = []
    train_actions = []
    for example in batch:
        if example.reward < reward_lower_bound:
            # skip this shit
            continue
        else:
            # the episode is overall good,
            # add its observations and actions to the learning pool
            train_observations.extend([
                step.observation
                for step in example.steps])
            train_actions.extend([
                step.action
                for step in example.steps])
    # make into nice shiny Tensors
    train_observations_v = torch.FloatTensor(train_observations)
    train_actions_v = torch.LongTensor(train_actions)

    return train_observations_v, train_actions_v, reward_lower_bound, reward_mean


def train_module(net, training_memory=None):
    """Main training function"""
    env = gym.make('CartPole-v0')

    # net = Net(128)
    objective = nn.CrossEntropyLoss()
    # fairly arbitrary optimiser, learning rate
    optimizer = optim.Adam(params=net.parameters(), lr=0.01)

    # skip the writing for now
    # writer = SummaryWriter(comment="-cartpole")

    episode_memory = []

    for iter_no, batch in enumerate(generate_training_data(env, net, 16)):
        # filter batch, get observations from it
        obs_v, acts_v, reward_b, reward_m = filter_batch(batch, 70.0)

        if training_memory is not None:
            # add the episodes to episode_memory
            episode_memory.extend(batch)
            # sort by reward
            episode_memory.sort(key=lambda x: x.reward)
            # retain only training_memory many
            episode_memory = episode_memory[ -training_memory: ]
            # 'filter' and use to train
            obs_v, acts_v, _, _ = filter_batch(episode_memory, 0.0)
        print(len(batch), len(obs_v))
        
        # do the optimiser dance
        optimizer.zero_grad()
        action_scores_v = net(obs_v)
        # compute the loss of the predictions
        loss_v = objective(action_scores_v, acts_v)
        loss_v.backward()
        optimizer.step()
        print(f"{iter_no}: loss={loss_v.item():.2f}, reward_mean={reward_m:.1f}, reward_bound={reward_b:.1f}")

        # writer.add_scalar("loss", loss_v.item(), iter_no)
        # writer.add_scalar("reward_bound", reward_b, iter_no)
        # writer.add_scalar("reward_mean", reward_m, iter_no)
        if reward_m > 199:
        # if reward_m > 80.0: # FIXME
            print("Solved!")
            break

    return iter_no, net


def compare_architectures():
    # make a few net types
    nets = [
        ('small', Net(64)),
        ('small_symmetric', Net(64, enforce_symmetry=True)),
        ('small_memory', Net(64)),
        ('small_symmetric_memory', Net(64, enforce_symmetry=True)),
        ('large', Net(128)),
        ('large_symmetric', Net(128, enforce_symmetry=True)),
        ('large_memory', Net(128)),
        ('large_symmetric_memory', Net(128, enforce_symmetry=True)),
        ]
    # train until success
    niters = []
    for label, net in nets:
        # niter, _ = train_module(net, training_memory=40)
        niter, _ = train_module(net,
                                training_memory=(16 if 'memory' in label else None))
        niters.append(niter)

    # plt.figure()
    # xs = np.arange(len(nets))
    # labels, _ = zip(*nets)
    # plt.bar(xs, niters)
    # plt.xticks(xs, labels, rotation=25, ha='right')
    for (label, _), niter in zip(nets, niters):
        print(label, niter)
        
    # display (only matplotlib for now sadly) number of required iterations


def loop_net(net):
    # get into a loop of continuously showing watsup
    sm = nn.Softmax(dim=1)
    env = gym.make('CartPole-v0')
    while True:
        is_done = False
        obs = env.reset()
        while not is_done:
            obs_v = torch.FloatTensor([obs])
            act_probs_v = sm(net(obs_v))
            act_probs = act_probs_v.data.numpy()[0]
            action = np.random.choice(2, p=act_probs)

            obs, reward, is_done, _ = env.step(action)

            env.render()

            time.sleep(0.005)
    env.close()

    # writer.close()



if __name__ == '__main__':
    # main()
    compare_architectures()
    # plt.show()
